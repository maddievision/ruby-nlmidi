Gem::Specification.new do |s|
  s.name        = 'nlmidi'
  s.version     = '0.0.5'
  s.date        = '2013-11-06'
  s.summary     = "NLMidi"
  s.description = "MIDI Library"
  s.authors     = ["Andrew Lim"]
  s.email       = 'andrew@nolimitzone.com'
  s.files       = ["lib/nlmidi.rb"]
  s.homepage    =
    'http://rubygems.org/gems/nlmidi'
  s.license       = 'MIT'
  s.add_runtime_dependency 'unimidi', '>= 0.3.3'
end
