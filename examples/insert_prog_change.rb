infn = "testout.mid"
outfn = "testout2.mid"

require "nlmidi"

seq = NLMidi::MidiSequence.new
w = NLMidi::MidiWriter.new
e = NLMidi::MidiEvents.new

seq.read infn

seq.tracks.each do |trk|

  newevents = [] #collect new program changes

  trk.events.each do |evt|
    if evt.data[0] & 0xF0 == STATUS_NOTE_ON and evt.data[2] > 0 #is note on (status = 0x9x and velocity > 0)

      ch = evt.data[0] & 0xF
      prg = 5

      #eid is used for event ordering, putting it before the note on ensures it gets 
      #written before the note on even though they have the same timestamp.

      #you can use an array instead of the e.program helper - like *[0xC0 + ch, prg]
      #or specify bytes as raw arguments

      newevent = NLMidi::MidiEvent.new evt.timestamp, evt.eid - 1, *e.program(ch,prg)
      newevents << newevent 

    end
  end

  #append new program changes
  newevents.each do |evt|
    trk.events << evt
  end 

end

w.write outfn, seq