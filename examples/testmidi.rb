require "nlmidi"

t = NLMidi::MidiTrack.new
p = NLMidi::MidiPlayer.new
w = NLMidi::MidiWriter.new

#custom macros monkey patch
class NLMidi::MidiTrack
    def prognote(ch,vel,len,prg,note) #program change + note
        self.program ch, prg
        self.note ch, vel, len, note
    end
end

port = 0
vel = 127

#timings
n1 = 96
n2 = 48
n4 = 24
n8 = 12

t.tempo 130

t.push #save time position (beginning)

#play chords C, F, G, C
ch = 0
t.program ch, 48
t.note ch, vel, n2, *[%W[C5 E5 G5], %W[C5 F5 A5], %W[D5 G5 B5], %W[E5 G5 C6]].to_notes

t.rewind #retrieve time position (beginning)

#play bass
ch = 1
t.program ch, 36
t.note ch, vel, n4, *%w[C3 C4 F3 F4 G3 G4 C3 C2].to_notes 

t.rewind #retrieve time position (beginning)

#instrument 8 play a melody
ch = 2
t.program ch, 71
t.note ch, vel, n8, *%w[E6 E6 G6 C7 F6 F6 A6 C7 B6 G6 A6 B6 C7 C7 C7].to_notes 

t.rewind #retrieve time position (beginning)

#drums (test prognote macro)
ch = 3
kick = [118, "A1".to_note] #inst 118, A1   - synth tom kick drum
snare = [127, "D6".to_note] #inst 127, D6  - gun shot snare.. lol

2.times do
    t.prognote ch, vel, n4, *kick
    t.prognote ch, vel, n4, *snare
    t.prognote ch, vel, n8, *kick
    t.prognote ch, vel, n8, *kick
    t.prognote ch, vel, n4, *snare
end

seq = NLMidi::MidiSequence.new 24,0,t

#play in real time
p.open port
p.gm_reset
p.all_notes_off
p.disable_reverb
p.play seq

#write to midi file
w.write "testout.mid", seq

