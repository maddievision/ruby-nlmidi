require "nlmidi"

p = NLMidi::MidiPlayer.new

p.open 0

p.gm_reset
p.disable_reverb
p.all_notes_off
