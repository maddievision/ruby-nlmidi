require "nlmidi"

p = NLMidi::MidiPlayer.new
s = NLMidi::MidiSequence.new

s.read "virt_-_Marry_Me_Bouche.mid"

#play in real time
p.open 0

p.gm_reset
p.disable_reverb
p.all_notes_off

sleep 0.5

p.play s
